<?php

namespace App\Form;

use App\Component\Form\Payload;
use App\Entity\Candle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PayloadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('o')
            ->add('c')
            ->add('h')
            ->add('l')
            ->add('v')
            ->add('time', DateTimeType::class, [
                'widget'   => 'single_text',
                'format'   => DateTimeType::HTML5_FORMAT,
                'required' => true,
            ])
            ->add('interval', ChoiceType::class, [
                'choices'      => Candle::CHOICES,
                'choice_value' => function (?int $value) {
                    return \array_keys(Candle::CHOICES)[$value] ?? null;
                }
            ])
            ->add('figi');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'         => Payload::class,
            'csrf_protection'    => false,
            'allow_extra_fields' => true,
        ]);
    }
}
