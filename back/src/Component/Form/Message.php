<?php

namespace App\Component\Form;

use App\Entity\Candle;

class Message
{
    /** @var null|string */
    private $event;

    /** @var null|Payload */
    private $payload;

    /**
     * @return string|null
     */
    public function getEvent(): ?string
    {
        return $this->event;
    }

    /**
     * @param string|null $event
     *
     * @return Message
     */
    public function setEvent(?string $event): Message
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return Payload|null
     */
    public function getPayload(): ?Payload
    {
        return $this->payload;
    }

    /**
     * @param Payload|null $payload
     *
     * @return Message
     */
    public function setPayload(?Payload $payload): Message
    {
        $this->payload = $payload;
        return $this;
    }

    public function toCandle(): Candle
    {
        if ('candle' !== $this->event) {
            throw new \InvalidArgumentException('Invalid event type.');
        }

        $payload = $this->getPayload();

        if (!$payload instanceof Payload) {
            throw new \InvalidArgumentException('Payload should not be null.');
        }

        return (new Candle())
            ->setInterval($payload->getInterval())
            ->setFigi($payload->getFigi())
            ->setVolume($payload->getV())
            ->setOpen($payload->getO())
            ->setClose($payload->getC())
            ->setHigh($payload->getH())
            ->setLow($payload->getL())
            ->setTime($payload->getTime());
    }
}
